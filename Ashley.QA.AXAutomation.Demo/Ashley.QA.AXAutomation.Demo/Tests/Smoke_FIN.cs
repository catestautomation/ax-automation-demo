﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using Ashley.QA.Automation.Framework.Windows;
using Ashley.QA.Automation.Framework.UIAutomationControl;
using Ashley.QA.Dynamics.AX2012.Automation.Utils;
using System.Text.RegularExpressions;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using Ashley.QA.Automation.Framework.Web;
using OpenQA.Selenium;

using Microsoft.VisualStudio.TestTools.UITest.Common.UIMap;
using System.IO;

using System.Data;
using System.Reflection;

namespace Ashley.QA.Dynamics.AX.Automation.Tests.SmokeTests
{
    [CodedUITest]
    [DeploymentItem(@"Utils\TestDriver", @"Utils\TestDriver")]
    [DeploymentItem(@"TestData", @"TestData")]
    public class Smoke_FIN
    {
        public Smoke_FIN()
        {

        }
        
       

        [TestMethod]
        [TestCategory("FIN")]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(91871)]
        [Description("Finance EOD Reports")]
        public void FinanceEodReports()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath_AshleyReport = "ECM/Accounts receivable/Reports/Ashley reports";
            string ARbalancing = "Accounts receivable/Reports/Ashley reports/AR Balancing";
            string ItemSalesDetails = "Accounts receivable/Reports/Ashley reports/Item Sales Detail";
            string CashReceiptJournalDetails = "Accounts receivable/Reports/Ashley reports/Cash receipt journal";
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;


            #endregion

            #region Local variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            WinWindow wnd_CreateCredit = new WinWindow();
            WinWindow wnd_Infolog = new WinWindow();
            WinWindow wnd_ISD = new WinWindow();
            WinWindow wnd_CRJ = new WinWindow();
            WinWindow wnd_ARBalancing = new WinWindow();
            HtmlRow row_Record = new HtmlRow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            WinEdit edit_discountPercentage = new WinEdit();
            WinButton btn_OK = new WinButton();

            WinButton btn_Select = new WinButton();
            WinButton btn_Ok = new WinButton();
            WinEdit edit_FromDate = new WinEdit();
            WinEdit edit_ToDate = new WinEdit();
            WinEdit edit_RetailChannel = new WinEdit();
            WinEdit edit_RetailType = new WinEdit();
            WinCheckBox checkBox_ShowTotal = new WinCheckBox();
            WinEdit edit_CustomerAccount = new WinEdit();

            #endregion

            #region Test Steps

            // 1) AX 2012 Sign on
            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);

            // 2) Navigate to ECM->Accounts Receivables->Reports->Ashley Reports->Item sales Detail
            wnd_AX.NavigateTo(AddressPath_AshleyReport);
            wnd_AX.SelectFromTreeItems(ItemSalesDetails);

            // 3) Verify the @Filters present in the Report Screen
            // 4) Enter the valid filter criteria and generate the report
            wnd_ISD = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "‪Item Sales Detail" }, PropertyExpressionOperator.Contains);
            edit_FromDate = wnd_ISD.DetectControl<WinEdit>(new { Name = "From date" });
            edit_FromDate.Text = DateTime.Now.AddDays(-2).ToString("dd/MM/yyyy");

            edit_ToDate = wnd_ISD.DetectControl<WinEdit>(new { Name = "To date" });
            edit_ToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

            btn_Ok = wnd_ISD.DetectControl<WinButton>(new { Name = "OK" });
            btn_Ok.WinClick();

            while (true)
            {
                try
                {
                    wnd_Infolog = UITestControl.Desktop.FindWinWindow(new { Name = "Infolog", ClassName = "AxTopLevelFrame" }, PropertyExpressionOperator.Contains);

                    if (wnd_Infolog.Exists)
                    { break; }
                }
                catch (Exception ex) { }
                wnd_ISD = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "‪Item Sales Detail" }, PropertyExpressionOperator.Contains);
                try {
                    WinToolBar toolBar = wnd_ISD.DetectControl<WinToolBar>(new { Name = "Toolstrip" });
                    if (toolBar.BoundingRectangle.Height > 0)
                    {
                        break;
                    }
                } catch (Exception ex) { }
            }

            wnd_ISD.CloseWindow();

            // 5) Navigate to ECM->Accounts Receivables->Reports->Ashley Reports->Cash receipt Journal report
            // 6) Enter the valid filter criteria and generate the report
            wnd_AX.NavigateTo(AddressPath_AshleyReport);
            wnd_AX.SelectFromTreeItems(CashReceiptJournalDetails);
            wnd_CRJ = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "Cash receipt journal" }, PropertyExpressionOperator.Contains);


            edit_FromDate = wnd_CRJ.DetectControl<WinEdit>(new { Name = "From date" });
            edit_FromDate.Text = DateTime.Now.AddMonths(-1).ToString("dd/MM/yyyy");

            edit_ToDate = wnd_CRJ.DetectControl<WinEdit>(new { Name = "To date" });
            edit_ToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

            btn_Ok = wnd_CRJ.DetectControl<WinButton>(new { Name = "OK" });
            btn_Ok.WinClick();

            wnd_CRJ = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "‪Cash receipt journal‬" }, PropertyExpressionOperator.Contains);
            wnd_CRJ.CloseWindow();

            // 7) Navigate to ECM->Accounts Receivables->Reports->Ashley Reports->AR balancing report
            // 8) Enter the valid filter criteria and generate the report
            wnd_AX.NavigateTo(AddressPath_AshleyReport);
            wnd_AX.SelectFromTreeItems(ARbalancing);

            wnd_ARBalancing = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "‪AR Balancing" }, PropertyExpressionOperator.Contains);

            edit_FromDate = wnd_ARBalancing.DetectControl<WinEdit>(new { Name = "From date" });
            edit_FromDate.Text = DateTime.Now.AddMonths(-1).ToString("dd/MM/yyyy");

            edit_ToDate = wnd_ARBalancing.DetectControl<WinEdit>(new { Name = "To date" });
            edit_ToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

            edit_RetailType = wnd_ARBalancing.DetectControl<WinEdit>(new { Name = "Report type" });
            edit_RetailType.Text = "Detail";

            checkBox_ShowTotal = wnd_ARBalancing.DetectControl<WinCheckBox>(new { Name = "Show total" });
            if (checkBox_ShowTotal.Checked == false)
            {
                checkBox_ShowTotal.WinClick();
            }

            btn_Ok = wnd_ARBalancing.DetectControl<WinButton>(new { Name = "OK" });
            btn_Ok.WinClick();

            //  Verify the data in the AR balancing report
            wnd_ARBalancing = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "‪AR Balancing" }, PropertyExpressionOperator.Contains);
            wnd_ARBalancing.CloseWindow();


            #endregion

            #region Test Cleanup

            #endregion
        }

       

        [TestMethod]
        [TestCategory("FIN")]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(107000)]
        [Description("FIN - Fraud report")]
        public void FinFraudReport()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string salesOrder = "ECM/Sales and Marketing/Reports/Sales orders";
            string OpenSaleOrderWithAddress = "Sales and Marketing/Reports/Sales orders/Open sales orders with addresses";

            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            WinWindow wnd_SC = new WinWindow();
            WinWindow wnd_OpenSaleOrderWithAddress = new WinWindow();
            WinWindow wnd_‪afiCustSalesOpenOrders = new WinWindow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            WinButton btn_Select = new WinButton();
            WinButton btn_Ok = new WinButton();
            WinEdit edit_CreationDate = new WinEdit();
            string creation_Date = DateTime.Today.ToString("dd-MM-yyyy");
            DateTime creationDate = Convert.ToDateTime(creation_Date);
            HtmlRow row_Record = new HtmlRow();
            Dictionary<String, String> dict_ColumnAndValues = new Dictionary<String, String>();
            #endregion


            #region Test Steps
            // 1, Login to AX

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);

            // 2, Click on the Sales and Marketing -> Reports -> sales orders->Open Sales Order with addresses 
            wnd_AX.NavigateTo(salesOrder);
            wnd_AX.SelectFromTreeItems(OpenSaleOrderWithAddress);

            // 3, click ->Select button and choose Created date from the Calendar and Click ok 
            wnd_OpenSaleOrderWithAddress = UITestControl.Desktop.FindWinWindow(new { Name = "‪Open sales orders with addresses‬ (‎‪1‬)‎", ClassName = "AxTopLevelFrame" });
            btn_Select = wnd_OpenSaleOrderWithAddress.DetectControl<WinButton>(new { Name = "Select" });
            btn_Select.WinClick();

            wnd_afiCustSalesOpenOrders = UITestControl.Desktop.FindWinWindow(new { Name = "‪afiCustSalesOpenOrders‬ (‎‪1‬ - ‎‪ecm‬)‎", ClassName = "AxPopupFrame" });
            dict_ColumnAndValues = new Dictionary<string, string>();
            dict_ColumnAndValues.Add("Field", "Creation date");
            wnd_afiCustSalesOpenOrders.EnterValueToCell("RangeGrid", dict_ColumnAndValues, "Criteria", creation_Date);
            btn_Ok = wnd_afiCustSalesOpenOrders.DetectControl<WinButton>(new { Name = "OK" });
            btn_Ok.WinClick();

            wnd_OpenSaleOrderWithAddress = UITestControl.Desktop.FindWinWindow(new { Name = "‪Open sales orders with addresses‬ (‎‪1‬)‎", ClassName = "AxTopLevelFrame" });
            edit_CreationDate = wnd_OpenSaleOrderWithAddress.DetectControl<WinEdit>(new { Name = "Creation date" });
            string creationDateText = edit_CreationDate.Text;
            string[] creationDateTime = creationDateText.Split('.');
            Assert.AreEqual(creationDate.AddDays(-1).AddHours(18).AddMinutes(00).AddSeconds(00).ToString("dd-MMM-yyyy hh:mm:ss tt").ProcessString(), creationDateTime[0].ProcessString(), "Time Mismatch");
            Assert.AreEqual(creationDate.AddHours(17).AddMinutes(59).AddSeconds(59).ToString("dd-MMM-yyyy hh:mm:ss tt").ProcessString(), creationDateTime[2].ProcessString(), "Time Mismatch");
            btn_Ok = wnd_OpenSaleOrderWithAddress.DetectControl<WinButton>(new { Name = "OK" });
            btn_Ok.WinClick();

            // 4, Click OK button in Open sales order with address pop -up

            wnd_OpenSaleOrderWithAddress = UITestControl.Desktop.FindWinWindow(new { Name = "‪Open sales orders with addresses‬ (‎‪1‬)‎", ClassName = "AxTopLevelFrame" });
            wnd_OpenSaleOrderWithAddress.CloseWindow();

            #endregion

            #region Test Cleanup

            #endregion
        }
   


        #region Additional test attributes

        [TestInitialize()]
        public void MyTestInitialize()
        {
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestContext.CustomTestCleanup();
        }

        #endregion

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}