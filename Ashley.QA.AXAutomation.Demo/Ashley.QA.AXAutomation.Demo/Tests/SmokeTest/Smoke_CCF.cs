﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using Ashley.QA.Automation.Framework.Windows;
using Ashley.QA.Automation.Framework.UIAutomationControl;
using Ashley.QA.Dynamics.AX2012.Automation.Utils;
using System.Text.RegularExpressions;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using Ashley.QA.Automation.Framework.Web;
using OpenQA.Selenium;
using Ashley.QA.Sitecore.Automation.SaleOrder;
using System.Threading;
using System.Diagnostics;
using System.IO;
using System.Data;

namespace Ashley.QA.Dynamics.AX.Automation.Tests.SmokeTests
{
    [CodedUITest]
    [DeploymentItem(@"Utils\TestDriver", @"Utils\TestDriver")]
    [DeploymentItem(@"TestData", @"TestData")]
    public class Smoke_CCF
    {
        public Smoke_CCF()
        {
        }
  
        public static string saleOrder;
        public string productCode;
        public string cardType;
        WinWindow wnd_AX = new WinWindow();
        public bool IsInvoiceNeeded = false;

        #region Sitecore order creation 

        public void CreateASaleOrderInSitecoreCCF()
        {
            DataTable dt_ = DynamicsAxCommonFunctions.ConvertCSVToDataTable();
            
            foreach (DataRow row in dt_.Rows)
            {
                #region TestData

                string url = row.Field<string>("URL") == null ? string.Empty : row.Field<string>("URL");

                string browserName = row.Field<string>("BrowserName") == null ? string.Empty : row.Field<string>("BrowserName");
                if (String.IsNullOrEmpty(productCode ))
                {
                    productCode = row.Field<string>("ProductCode") == null ? string.Empty : row.Field<string>("ProductCode");
                }
                string quantity = row.Field<string>("Quantity") == null ? string.Empty : row.Field<string>("Quantity"); 
                string zipCode = row.Field<string>("ZipCode") == null ? string.Empty : row.Field<string>("ZipCode"); 
                string firstName = row.Field<string>("FirstName") == null ? string.Empty : row.Field<string>("FirstName");
                string lastName = row.Field<string>("Lastname") == null ? string.Empty : row.Field<string>("Lastname");
                string address = row.Field<string>("Address") == null ? string.Empty : row.Field<string>("Address");
                string city = row.Field<string>("City") == null ? string.Empty : row.Field<string>("City");
                string state = row.Field<string>("State") == null ? string.Empty : row.Field<string>("State"); 
                string email = row.Field<string>("Email") == null ? string.Empty : row.Field<string>("Email");
                string phoneNo = row.Field<string>("PhoneNumber") == null ? string.Empty : row.Field<string>("PhoneNumber"); 
                cardType = String.IsNullOrEmpty(cardType ) ? "credit" : cardType;
                string cardNo = row.Field<string>("CardNo") == null ? string.Empty : row.Field<string>("CardNo").Replace("'", ""); 
                string cvv = row.Field<string>("SecurityCode") == null ? string.Empty : row.Field<string>("SecurityCode"); 
                string expMonth = DateTime.Today.AddMonths(1).Date.Month.ToString();
                string expYear = DateTime.Today.AddYears(1).Date.Year.ToString();

                #endregion

                #region Sale Order creation
                IsInvoiceNeeded = false;
                
                if (string.IsNullOrEmpty(saleOrder))
                {
                   
                    saleOrder = SitecoreSO.CreateSaleOrder(url, browserName, productCode, quantity, zipCode, firstName, lastName, address, city, state, phoneNo, email, cardType, cardNo, cvv, expMonth, expYear);
                    IsInvoiceNeeded = true;
                }

                #endregion
            }

        }
        

        #endregion

       

        [TestMethod]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(91888)]
        [Description("Apply Discounts - Header level / Line level")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://aarcwivpap10797:8080/tfs/Ashley;Ashley.QA", "91888", DataAccessMethod.Sequential)]
        public void ApplyDiscountsHeaderLevelLineLevel()
        {
            int iterationCount = TestContext.DataRow.Table.Rows.IndexOf(TestContext.DataRow);
            if (iterationCount == 0)
            {
                #region Test Data

                string PurchaseOrder = String.Empty;
                string LineLevelDiscountAmount = TestContext.DataRow["LLDA"].ToString();
                string LineLevelDiscountReasonCode = "Third Party Vendor";//TestContext.DataRow["LLDAReasoncode"].ToString();
                string HeaderLevelDiscountPercentage = TestContext.DataRow["HLDP"].ToString();
                string HeaderLevelDiscountReasonCode = TestContext.DataRow["HLDPReasoncode"].ToString();

                string Path = AX.Automation.Properties.Settings.Default.AX_Path;
                string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;

                #endregion

                #region Local Variables
                WinWindow wnd_SO = new WinWindow();
                UITestControlCollection uitcc_SOLines = new UITestControlCollection();
                WinWindow wnd_SODiscount = new WinWindow();
                WinEdit edit_DicountAmount = new WinEdit();
                WinButton btn_ApplyAndClose = new WinButton();
                WinWindow wnd_DH = new WinWindow();
                UITestControlCollection uitcc_DHLines = new UITestControlCollection();

                List<String> list_InfologMessages = new List<String>();
                List<String> list_ExpectedDiscountMessages = new List<String>();

                #endregion

                #region Pre-requisite

                //  1, Use the Sales order from Testcase #121476
                //     CreateASaleOrderInSitecoreCCF();
                saleOrder = "725000144596";
               
                #endregion

                #region Test Steps

                //  2, Go to AX application - Sale Order form
                wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
                wnd_AX.NavigateTo(AddressPath);
                wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
                wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);

                //  3, Apply line level discount in $ (positive) for the sales order
                //  3.1, Go to Sales order tab and Click on Modify in action pane
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Modify");
                Assert.IsNotNull(wnd_SO.FindRibbonMenuItem("Sales order", "Maintain", "Complete"), "Sale Order is not editable.");

                //  3.2, Select the main line and Click Add line discount (right side drop down)
                //  3.3, Enter @LineLevelDisountAmount in Manual Disount $, Select the @LineleveldiscountReasoncode, Click on Apply and Close
                //  3.4, Click Close in infolog Infolog should get closed
                //  3.5, Note: Repeat from Step 2 to Step 4 for all the main lines available in the selected Sales order form
                //wnd_SO.ApplyColumnFilter("SalesLineGrid", "Line stage", "Is not", "Delivery charge");
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery charge"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String itemNumber = row_SOLine.GetColumnValueOfARecord("Item number");
                    Double origQty = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Orig qty"));
                    Double bd_NetAmount = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Net amount"));
                    Double discountAmount = (Convert.ToDouble(LineLevelDiscountAmount) / origQty);

                    row_SOLine.SelectARecord(true, false);
                    wnd_SO.ClickTableMenuItem("LineOverviewActionTab", "Add line discount", null);

                    wnd_SODiscount = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order discount:", ClassName = "AxTopLevelFrame" });
                    edit_DicountAmount = wnd_SODiscount.DetectControl<WinEdit>(new { Name = "Manual discount $" });
                    edit_DicountAmount.EnterWinTextV2(LineLevelDiscountAmount);
                    wnd_SODiscount.SelectValueFromCustomDropdown("Reason code", "Default comment", LineLevelDiscountReasonCode);
                    btn_ApplyAndClose = wnd_SODiscount.DetectControl<WinButton>(new { Name = "Apply and Close" });
                    btn_ApplyAndClose.WinClick();
                    list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
                    Assert.IsFalse(list_InfologMessages.Contains("One or more critical STOP errors have occurred. Use the error messages below to guide you or call your administrator."), String.Join("\n", list_InfologMessages));

                    try
                    {
                        String discountMessage = list_InfologMessages.First(x => x.StartsWith("Discount amount $" + String.Format("{0:0.00}", discountAmount) + " per unit, qty " + origQty + ", for a total of $" + String.Format("{0:0.00}", Convert.ToDouble(LineLevelDiscountAmount)) + " for item " + itemNumber));
                    }
                    catch
                    {
                        Assert.Fail("'" + "Discount amount $" + String.Format("{0:0.00}", discountAmount) + " per unit, qty " + origQty + ", for a total of $" + String.Format("{0:0.00}", Convert.ToDouble(LineLevelDiscountAmount)) + " for item " + itemNumber + "' Message is not available in the following list \n" + String.Join("\n", list_InfologMessages));
                    }

                    Double ad_NetAmount = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Net amount"));
                    Assert.AreEqual(Math.Round(bd_NetAmount - Convert.ToDouble(LineLevelDiscountAmount), 2), ad_NetAmount, "Net amount for Item number '" + itemNumber + "' is not calculated properly after applying Line Level Discount Amount: $" + String.Format("{0:0.00}", Convert.ToDouble(LineLevelDiscountAmount)));
                    row_SOLine.SelectARecord(false, false);
                }

                //  3.6, Click Complete in action pane
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Complete");
                Assert.IsNotNull(wnd_SO.FindRibbonMenuItem("Sales order", "Maintain", "Modify"), "Modify Button is not visible.");

                //  3.7, Click Close in infolog
                list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
                Assert.IsFalse(list_InfologMessages.Contains("One or more critical STOP errors have occurred. Use the error messages below to guide you or call your administrator."), String.Join("\n", list_InfologMessages));

                //  4, Apply header level discount in % (positive) for the sales order
                //  4.1, Go to Sales order tab and Click on Modify in action pane
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Modify");
                Assert.IsNotNull(wnd_SO.FindRibbonMenuItem("Sales order", "Maintain", "Complete"), "Sale Order is not editable.");

                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery charge"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String itemNumber = row_SOLine.GetColumnValueOfARecord("Item number");
                    Double origQty = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Orig qty"));
                    Double bd_NetAmount = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Net amount"));
                    Double discountAmount = Math.Round((bd_NetAmount * (Convert.ToDouble(HeaderLevelDiscountPercentage) / 100) / origQty), 2);
                    Double total = Math.Round(bd_NetAmount * (Convert.ToDouble(HeaderLevelDiscountPercentage) / 100), 2);

                    list_ExpectedDiscountMessages.Add("Discount amount $" + String.Format("{0:0.00}", discountAmount) + " per unit, qty " + origQty + ", for a total of $" + String.Format("{0:0.00}", total) + " for item " + itemNumber);
                }

                //  4.2, Click Manual discounts => Sales order discount in action pane
                wnd_SO.ClickRibbonMenuItem("SalesOrder", "Manual discounts", "Sales order discount");

                //  4.3, Enter @HeaderLevelDisountPercentage in Manual Disount %, Select the @HeaderleveldisountReasoncode, Click on Apply and Close
                wnd_SODiscount = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order discount:", ClassName = "AxTopLevelFrame" });
                edit_DicountAmount = wnd_SODiscount.DetectControl<WinEdit>(new { Name = "Manual discount %" });
                edit_DicountAmount.EnterWinTextV2(HeaderLevelDiscountPercentage);
                wnd_SODiscount.SelectValueFromCustomDropdown("Reason code", "Default comment", HeaderLevelDiscountReasonCode);
                btn_ApplyAndClose = wnd_SODiscount.DetectControl<WinButton>(new { Name = "Apply and Close" });
                btn_ApplyAndClose.WinClick();

                //  4.4, Click Close in infolog
                list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
                Assert.IsFalse(list_InfologMessages.Contains("One or more critical STOP errors have occurred. Use the error messages below to guide you or call your administrator."), String.Join("\n", list_InfologMessages));

                foreach (String message in list_ExpectedDiscountMessages)
                {
                    try
                    {
                        String discountMessage = list_InfologMessages.First(x => x.StartsWith(message));
                    }
                    catch
                    {
                        Assert.Fail("'" + message + "' Message is not available in the following list \n" + String.Join("\n", list_InfologMessages));
                    }
                }

                //  4.5, Click Complete in action pane            
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Complete");
                Assert.IsNotNull(wnd_SO.FindRibbonMenuItem("Sales order", "Maintain", "Modify"), "Modify Button is not visible.");

                //  4.6, Click Close in infolog
                list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
                Assert.IsFalse(list_InfologMessages.Contains("One or more critical STOP errors have occurred. Use the error messages below to guide you or call your administrator."), String.Join("\n", list_InfologMessages));

                //  5, Select the main line in SO form Click Discount history (right side drop down)
                //  6, Check the discount history
                //  7, Click Close in Discount history page
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                //wnd_SO.ApplyColumnFilter("SalesLineGrid", "Line stage", "Is not", "Delivery charge");            
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery charge"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    row_SOLine.SelectARecord(true, false);
                    wnd_SO.ClickTableMenuItem("LineOverviewActionTab", "Discount history", null);

                    //  6,
                    wnd_DH = UITestControl.Desktop.FindWinWindow(new { Name = "Discount history:", ClassName = "AxTopLevelFrame" });
                    uitcc_DHLines = wnd_DH.GetAllRecords("ManualDiscounts");
                    uitcc_DHLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Off"));

                    //Before
                    Double b_NetAmountPriorToDiscount = Convert.ToDouble(((WinRow)uitcc_DHLines[uitcc_DHLines.Count - 1]).GetColumnValueOfARecord("Net amount prior to discount"));
                    Double b_DiscountPercentage = Convert.ToDouble(((WinRow)uitcc_DHLines[uitcc_DHLines.Count - 1]).GetColumnValueOfARecord("Discount %"));
                    Double b_DiscountAmount = Convert.ToDouble(((WinRow)uitcc_DHLines[uitcc_DHLines.Count - 1]).GetColumnValueOfARecord("Discount $"));
                    String b_DiscountDescription = ((WinRow)uitcc_DHLines[uitcc_DHLines.Count - 1]).GetColumnValueOfARecord("Discount description");
                    Double b_NetAmountOfSalesLine = Convert.ToDouble(((WinRow)uitcc_DHLines[uitcc_DHLines.Count - 1]).GetColumnValueOfARecord("Net amount of sales line"));

                    Assert.AreEqual(0, b_DiscountPercentage, "Discount History Before Applying Discount: Discount % is " + b_DiscountPercentage);
                    Assert.AreEqual(0, b_DiscountAmount, "Discount History Before Applying Discount: Discount $ is " + b_DiscountAmount);
                    Assert.AreEqual("(null)", b_DiscountDescription, "Discount History Before Applying Discount: Discount Description is " + b_DiscountDescription);

                    //After Line Level
                    Double l_NetAmountPriorToDiscount = Convert.ToDouble(((WinRow)uitcc_DHLines[1]).GetColumnValueOfARecord("Net amount prior to discount"));
                    Double l_DiscountAmount = Convert.ToDouble(((WinRow)uitcc_DHLines[1]).GetColumnValueOfARecord("Discount $"));
                    String l_DiscountDescription = ((WinRow)uitcc_DHLines[1]).GetColumnValueOfARecord("Discount description");
                    Double l_NetAmountOfSalesLine = Convert.ToDouble(((WinRow)uitcc_DHLines[1]).GetColumnValueOfARecord("Net amount of sales line"));

                    Assert.AreEqual(l_NetAmountOfSalesLine + l_DiscountAmount, l_NetAmountPriorToDiscount, "Discount History After Applying Line Level Discount: Net amount prior to discount is " + l_NetAmountPriorToDiscount);
                    Assert.AreEqual(Convert.ToDouble(LineLevelDiscountAmount), l_DiscountAmount, "Discount History After Applying Line Level Discount: Discount $ is " + l_DiscountAmount);
                    Assert.AreEqual(LineLevelDiscountReasonCode, l_DiscountDescription, "Discount History After Applying Line Level Discount: Discount Description is " + l_DiscountDescription);
                    Assert.AreEqual(Math.Round((l_NetAmountPriorToDiscount - l_DiscountAmount), 2), l_NetAmountOfSalesLine, "Discount History After Applying Line Level Discount: Net amount of sales line is " + l_NetAmountOfSalesLine);

                    //After Header Level
                    Double h_NetAmountPriorToDiscount = Convert.ToDouble(((WinRow)uitcc_DHLines[0]).GetColumnValueOfARecord("Net amount prior to discount"));
                    Double h_DiscountPercentage = Convert.ToDouble(((WinRow)uitcc_DHLines[0]).GetColumnValueOfARecord("Discount %"));
                    String h_DiscountDescription = ((WinRow)uitcc_DHLines[0]).GetColumnValueOfARecord("Discount description");
                    Double h_NetAmountOfSalesLine = Convert.ToDouble(((WinRow)uitcc_DHLines[0]).GetColumnValueOfARecord("Net amount of sales line"));

                    Assert.AreEqual(l_NetAmountOfSalesLine, h_NetAmountPriorToDiscount, "Discount History After Applying Header Level Discount: Net amount prior to discount is " + h_NetAmountPriorToDiscount);
                    Assert.AreEqual(Convert.ToDouble(HeaderLevelDiscountPercentage), h_DiscountPercentage, "Discount History After Applying Header Level Discount: Discount % is " + h_DiscountPercentage);
                    Assert.AreEqual(HeaderLevelDiscountReasonCode, h_DiscountDescription, "Discount History After Applying Header Level Discount: Discount Description is " + h_DiscountDescription);
                    Assert.AreEqual(Math.Round((h_NetAmountPriorToDiscount - (h_NetAmountPriorToDiscount * h_DiscountPercentage / 100))),Math.Round(h_NetAmountOfSalesLine), "Discount History After Applying Header Level Discount: Net amount of sales line is " + h_NetAmountOfSalesLine);

                    //  7,
                    wnd_DH.CloseWindow();
                    row_SOLine.SelectARecord(false, false);
                }


                #endregion

                wnd_SO.CloseWindow();

                #region Test Cleanup

              
                #endregion
            }
        }

    

        [TestMethod]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(91891)]
        [Description("Exchange Process")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://aarcwivpap10797:8080/tfs/Ashley;Ashley.QA", "91891", DataAccessMethod.Sequential)]
        public void ExchangeProcess()
        {
            int iterationCount = TestContext.DataRow.Table.Rows.IndexOf(TestContext.DataRow);
            if (iterationCount == 0)
            {

                #region Test Data

                string PurchaseOrder = TestContext.DataRow["PurchaseOrder"].ToString();
                string Path = AX.Automation.Properties.Settings.Default.AX_Path;
                string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;

                string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
                string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
                string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;

                //string adjustmentType = "";
                //string reasonCode = "";
                string adjustmentType = TestContext.DataRow["AdjustmentType"].ToString();
                string reasonCode = TestContext.DataRow["ReasonCode"].ToString();

                string eXMLStagePOCreationPending = TestContext.DataRow["EXMLStagePOCreationPending"].ToString();
                string eXMLStatusPOCreationPending = TestContext.DataRow["EXMLStatusPOCreationPending"].ToString();

                string eXMLStageUnscheduled = TestContext.DataRow["EXMLStageUnscheduled"].ToString();
                string eXMLStatusUnscheduled = TestContext.DataRow["EXMLStatusUnscheduled"].ToString();

                string eXMLStageScheduled = TestContext.DataRow["EXMLStageScheduled"].ToString();
                string eXMLStatusScheduled = TestContext.DataRow["EXMLStatusScheduled"].ToString();

                string eXMLStageInvoiced = TestContext.DataRow["EXMLStageInvoiced"].ToString();
                string eXMLStatusInvoiced = TestContext.DataRow["EXMLStatusInvoiced"].ToString();

                //string itemCode = TestContext.DataRow["ItemCode"].ToString();
                string exchangeQty = TestContext.DataRow["ExchangeQuantity"].ToString();
                string returnQty = TestContext.DataRow["ReturnQuantity"].ToString();
                string exchangeOrderNumber = string.Empty;

                #endregion

                #region Local Variables
                WinWindow wnd_AX = new WinWindow();
                WinWindow wnd_SO = new WinWindow();
                WinWindow wnd_CreateCredit = new WinWindow();
                WinWindow wnd_RMAForm = new WinWindow();
                WinWindow wnd_FindReplacementItem = new WinWindow();
                BrowserWindow browserWindow = new BrowserWindow();
                HtmlRow row_Record = new HtmlRow();
                UITestControlCollection uitcc_SOLines = new UITestControlCollection();
                WinEdit edit_discountPercentage = new WinEdit();
                WinButton btn_OK = new WinButton();
                string lineConfirmationId = string.Empty;
                string ItemCode = string.Empty;
                WinButton btn_ExchangeItem = new WinButton();
                IWebDriver driver = null;
                string ERPNumber = string.Empty;

                #endregion

                #region Pre-requisite
                //  1, Create Sales Order using credit card payment type
                CreateASaleOrderInSitecoreCCF();

                PurchaseOrder = saleOrder + 1.ToString("00");
                #endregion

                #region Test Steps
                // 2, Go to AX application - Sale Order form
                wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
                wnd_AX.NavigateTo(AddressPath);
                wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
                wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

                #region Invoice Process
                if (IsInvoiceNeeded)
                {
                    wnd_SO.WaitForPurchaseOrderToBeGenerated();
                    driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
                    driver = driver.SelectTab("Homestore portal");
                    driver.SelectFromRibbon("Purchase order", "Stages", "New");
                    driver = driver.PortalFilter("Purchase order", PurchaseOrder);
                    driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");
                    ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
                    driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");


                    // 12.2, Search and click on the purchase order
                    // 12.3, Click Invoice Process => New Vendor Invoice => Post Invoice   
                    driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");
                    driver = driver.PortalFilter("Purchase order", PurchaseOrder);
                    driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");
                    driver.Invoice();


                }
                #endregion

                // 3, Go to Sell tab and click Credit note
                wnd_SO.ClickRibbonMenuItem("Sell", "Create", "Credit note");
                wnd_CreateCredit = UITestControl.Desktop.FindWinWindow(new { Name = "Create credit note:", ClassName = "AxTopLevelFrame" });

                // 4, Select @AdjustmentType and Select the @ReasonCode
                wnd_CreateCredit.SelectValueFromWinCombobox("Adjustment type", adjustmentType);
                wnd_CreateCredit.SelectValueFromCustomDropdown("Reason code", "Reason code", reasonCode);

                // 5, Select the original invoiced sales line in upper pane
                wnd_CreateCredit.CheckInsideACell("Invoice_Heading", new Dictionary<string, string>() { { "Sales order", saleOrder } }, "Select all", false);
                // Modify the @ReturnQuantity in Quantity column (Main line) in grid(lower pane) and press Enter key
                uitcc_SOLines = wnd_CreateCredit.GetAllRecords("Invoice_Lines");
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    ItemCode = row_SOLine.GetColumnValueOfARecord("Item");
                    // 6, Highlight a line (item where Return Qty is modified) in lower pane
                    // Click Exchange item button
                    wnd_CreateCredit.EnterValueToCell("Invoice_Lines", new Dictionary<string, string>() { { "Item", ItemCode } }, "Quantity", returnQty);
                    btn_ExchangeItem = wnd_CreateCredit.DetectControl<WinButton>(new { Name = "Exchange item" });
                    btn_ExchangeItem.WinClick();
                    btn_ExchangeItem.WinClick();

                    // 7, Enter @ExchangeQuantity in Exchange Quantity column and @ReturnQuantity in Return quantity column
                    // Click on Apply
                    wnd_FindReplacementItem = UITestControl.Desktop.FindWinWindow(new { Name = "Find replacement item:", ClassName = "AxTopLevelFrame" });
                    wnd_FindReplacementItem.SelectARecord("GridExistingItems", new Dictionary<string, string>() { { "Item number", ItemCode } }, true, false);
                    wnd_FindReplacementItem.EnterValueToCell("GridExistingItems", new Dictionary<string, string>() { { "Item number", ItemCode } }, "Return quantity", returnQty);
                    wnd_FindReplacementItem.EnterValueToCell("GridExistingItems", new Dictionary<string, string>() { { "Item number", ItemCode } }, "Exchange qty", exchangeQty);

                    WinButton btn_Apply = (WinButton)wnd_FindReplacementItem.DetectControl<WinButton>(new { Name = "Apply" });
                    btn_Apply.WinClick();
                }
               

                // 8, Click OK in Create credit note form
                btn_OK = wnd_CreateCredit.DetectControl<WinWindow>(new { ClassName = "AxButton" }).DetectControl<WinButton>(new { Name = "OK" });
                btn_OK.WinClick();

                // 9, Click Close in RMA form
                // wnd_SO.CloseWindow();
                wnd_RMAForm = UITestControl.Desktop.FindWinWindow(new { Name = "Return order  -  RMA number:", ClassName = "AxTopLevelFrame" });
                wnd_RMAForm.CloseWindow();
                UITestControlCollection coll = null;
                while (true)
                {
                    // 11,Click Complete in Return SO form
                    coll = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "Sales order", ClassName = "AxTopLevelFrame" }, PropertyExpressionOperator.Contains).FindMatchingControls();
                    if(coll.Count > 1)
                    {
                        break;
                    }
                }

                wnd_SO = (WinWindow)coll.First(x => !x.Name.Contains(saleOrder));
                wnd_SO.ClickRibbonMenuItem("Main", "Maintain", "Complete");

                // Click Close in infolog
                List<string> lst_ActualValue = DynamicsAxCommonFunctions.HandleInfolog(true);
                Assert.IsFalse(lst_ActualValue.Contains("Error"), "Error applying Line discount for the products");

                // 13, Go to the Retun [Exchange] Sales order form
                //wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

                // 14, Select the lines in Sale order lines grid
                // Verify the Line Stage and Line Status of the lines
                wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });              
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(adjustmentType));
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage").ProcessString();
                    String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status").ProcessString();
                    exchangeOrderNumber = row_SOLine.GetColumnValueOfARecord("Line confirmation ID");

                    Assert.AreEqual(eXMLStagePOCreationPending.ProcessString(), lineStage, PurchaseOrder + " PO creation pending");
                    Assert.AreEqual(eXMLStatusPOCreationPending.ProcessString(), lineStatus, PurchaseOrder + " Po creation pending");
                }

                // 15, Wait till PO is auto generated for the Return SO
                // Enusre that PO is created for the Return Sales Order
                wnd_SO.WaitForPurchaseOrderToBeGenerated();

                // 16, Select the lines in Sale order lines grid
                // Verify the Line Stage and Line Status of the lines
                wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(adjustmentType));
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage").ProcessString();
                    String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status").ProcessString();

                    Assert.AreEqual(eXMLStageUnscheduled.ProcessString(), lineStage, PurchaseOrder + "PO creation pending");
                    Assert.AreEqual(eXMLStatusUnscheduled.ProcessString(), lineStatus, PurchaseOrder + "Po creation pending");
                }

                // 17, Schedule the DS lines via AS400
                // 18, Schedule the HD lines via Portal
                // 19, Go to PTL application
                driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);

                // 20, Schedule the HD Negative (Return) Purchase Order(s) via Homestore Portal (PTL)
                // 20.1 Click HomeStore portal => Purchase order => New Returns cue icon
                driver = driver.SelectTab("Homestore portal");
                driver.SelectFromRibbon("Purchase order", "Stages", "New returns");


                // 20.2, Seach and click on the negative purchase order
                driver = driver.PortalFilter("Purchase order", exchangeOrderNumber);
                driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");


                // 20.3 Enter the @NPOERPnumber & @NPOConfirmeddeliverydate
                // Click Apply to lines
                // 20.4, Click Save and Close
                ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
                driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");

                // 21, Go to AX application - Return Sale Order form
                //wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                wnd_SO.SetFocus();
                // 22, Select the lines in Sale order lines grid
                // Verify the Line Stage and Line Status of the lines
                wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(adjustmentType));
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage").ProcessString();
                    String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status").ProcessString();

                    Assert.AreEqual(eXMLStageScheduled.ProcessString(), lineStage, PurchaseOrder + "Line stage mismatch");
                    Assert.AreEqual(eXMLStatusScheduled.ProcessString(), lineStatus, PurchaseOrder + "Line status mismatch");
                }

                // 23, Invoice the DS lines via AS400
                // 24, Invoice the HD lines via Portal
                // 25, Go to PTL application           
                // 26, Portal: Invoice the Return (Negative) Purchase Order
                // 26.1, Click on the Scheduled returns cue icon
                driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled returns");
                driver = driver.PortalFilter("Purchase order", exchangeOrderNumber);
                driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

                // 26.3, Click Invoice Process => New Vendor Invoice => Post Invoice
                driver.Invoice();

                // 27, Go to AX application -Sale Order form
                // wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });

                // 28, Select the lines in Sale order lines grid
                // Verify the Line Stage and Line Status of the lines
                wnd_SO.SetFocus();
                wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(PurchaseOrder));
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String lineStage = row_SOLine.GetColumnValueOfARecord("Line stage").ProcessString();
                    String lineStatus = row_SOLine.GetColumnValueOfARecord("Line status").ProcessString();

                    Assert.AreEqual(eXMLStageInvoiced.ProcessString(), lineStage, PurchaseOrder + "Line stage mismatch");
                    Assert.AreEqual(eXMLStatusInvoiced.ProcessString(), lineStatus, PurchaseOrder + "Line status mismatch");
                }

                wnd_SO.CloseWindow();
                #endregion
                saleOrder = string.Empty;
                #region Test Cleanup

                #endregion

            }
        }

        #region Additional test attributes

        [TestInitialize()]
        public void MyTestInitialize()
        {
            
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestContext.CustomTestCleanup();
        }

        #endregion

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
